
The Taxonomy Link Mover is a relatively simple module that shifts taxonomy term links
from the theme variable $links to the $content variable. This allows the administrator
to tell the CCK or Section Order modules to re-order where it is displayed.

An option still allows for other modules to apply hook_link_alter to the links. So,
for example, Taxonomy Image still has a chance to insert images representing each term.

The settings page alows you to choose which vocabularies to move. If you choose not to
move some, then they will still be included in $terms. This gives you additional
flexibility in formatting and ordering your content display.

Other options allow you to sort, or re-order, the vocabularies and/or terms by ID or
name. One caveat here, though, if you specify re-ordering of the terms, then all terms,
in all vocabularies, will be sorted, whether this module is processing that vocabulary
or not.

If CCK is available, this module will also supply the content section as an extra field
so that it can be reordered with CCK.

The true capability of this module comes through when coupled with the Section Order
module. With that module also in place, you can have one vocabulary's links displayed
above the body and another's below the body.

Note that this type of function is already available in Drupal 7, so I do not anticipate
porting this module to 7.x.